## Bootstrap Server

Bootstrap Server is an Ansible playbook to automate the initial setup of CentOS servers. It creates a deploy user for future Ansible playbooks and hardens the server according to standard security protocols. When you have updated the hosts file, run the playbook with the following command `ansible-playbook -i hosts tasks/main.yml --ask-pass`.

## Known Issues

* The command used to generated the cryped password for the deploy user must be run on a Linux box. Linux is the only platform that produces the correct result for the shadow file.
* The `seport` module is marked as experimental. This playbook uses the `command` module to manually set the SSH port in SELinux.
* The `firewalld` module is marked as experimental. There is an incompatibility between this module and the `service` module. This playbook uses the `command` module to manually enable and start the firewalld service.
